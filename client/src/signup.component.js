import React, { Component } from "react";
import axios from 'axios';

export default class SignUp extends Component {
  
    state = {
        firstName: '',
        lastName: '',
        password: '',
        error: '',
      }
    
      handleChange1 = event => {
        this.setState({ firstName: event.target.value });
      }
      handleChange2 = event => {
        this.setState({ lastName: event.target.value });
      }
      handleChange3 = event => {
        this.setState({ password: event.target.value });
      }
    
      handleSubmit = event => {
        event.preventDefault();
    
        const user = {
            'id':this.props.account,
            'firstName': this.state.firstName,
            'lastName': this.state.lastName,
            'password': this.state.password,
            "contractAddress": this.props.contractAddress
        };
        console.log(user);
        axios.post(`https://localhost:5000/api/Users/Register`,  user )
          .then(res => {
            console.log(res);
            console.log(res.data);
            window.location.assign("/");
          })
          .catch(err => {
            console.error(err); 
            if(err.status == 400){
            }
            this.setState({error: 'You account has already in system'})
          })
      }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label class='text-login-color'>First name</label>
                    <input type="text" ref={(input) => { this.productName = input }} name="firstName" onChange={this.handleChange1} className="form-control input-" placeholder="First name" />
                </div>

                <div className="form-group">
                    <label class='text-login-color'>Last name</label>
                    <input type="text" ref={(input) => { this.productName = input }} name="lastName" onChange={this.handleChange2} className="form-control input-" placeholder="Last name" />
                </div>

                
                <div className="form-group">
                    <label class='text-login-color'>ACCOUNT ADDRESS</label>
                    <input type="text" className="form-control" placeholder="Public Key" name="id" readOnly value={this.props.account} />
                </div>

                <div className="form-group">
                    <label class='text-login-color'>Password</label>
                    <input type="password" ref={(input) => { this.productName = input }} name="password" onChange={this.handleChange3} className="form-control input-" placeholder="Enter password" />
                </div>
                <p style={{color: "red"}}>{this.state.error}</p>
                <button type="submit" class="btn btn-light btn-block login-button">Register</button>
                
            </form>
            
        );
    }
}