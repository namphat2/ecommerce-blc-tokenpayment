import React, { Component } from 'react';

import imgs from './assets/images/Marketplace.png'
import { BrowserRouter as Router, Switch, Route, Link, HashRouter } from "react-router-dom";
import Cookies from 'js-cookie';
import ListProduct from './pages/listProduct';
import ProductItMe from './pages/productItMe';
import PurchasedProduct from './pages/purchasedProduct';
// import OnProduct from './pages/products';
import CreateProduct from './pages/createProduct';
import SoldProduct from './pages/soldProduct';

import BrowseKYC from './pages/browseKYC';
import BuyToken from './pages/buyToken';
import {FaShopify,FaWpforms,FaCoins ,FaCartArrowDown,FaCartPlus,FaShoppingCart ,FaHandHoldingUsd,FaCheck} from "react-icons/fa";
import Avatar from "@material-ui/core/Avatar";
class Sidenav extends Component {
  logOut() {
    Cookies.remove('token')
    window.location.assign('/');
  }
  render() {
    return (
      <Router>
        <body class="bg-theme bg-theme1">
          <div id="wrapper">
            <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
              <div class="brand-logo">
                <a href="/">
                  <img src={imgs} class="logo-icon" alt="logo icon" />
                  <h5 class="logo-text" style={{ fontWeight: 'bold' }}>Ecommerce-blc</h5>
                </a>
              </div>

              <ul class="sidebar-menu do-nicescrol">
                <li class="sidebar-header">MAIN NAVIGATION</li>
                
                {this.props.isActive ? <li class="nav-item dropdown-lg">
                  
                  <a class=" dropdown-toggle dropdown-toggle-nocaret waves-effect" style={{marginLeft:10}} data-toggle="dropdown" href="javascript:void();">
                   My Token: {this.props.token} <FaCoins/>&emsp;</a>
                </li> : <></>}
                <li>
                  <Link to={"/"}>
                    <i class="zmdi zmdi-view-dashboard"></i> <FaWpforms class='iCons'/><span> List Product</span>
                  </Link>
                </li>
                {this.props.isActive ? <><li class="sidebar-header">PRIVATE</li>
                  <li><Link to={"/product-it-me"}><i class="zmdi zmdi-coffee text-danger"></i> <FaCartArrowDown class='iCons'/><span> My Product</span></Link></li>
                  <li><Link to={"/purchased-products"}><i class="zmdi zmdi-chart-donut text-success"></i> <FaCartPlus class='iCons'/><span> Purchased Products</span></Link></li>
                  <li><Link to={"/sold-products"}><i class="zmdi zmdi-chart-donut text-success"></i> <FaShoppingCart class='iCons'/><span> Products Sold</span></Link></li>
                  <li><Link to={"/buy-tokens"}><i class="zmdi zmdi-chart-donut text-success"></i> <FaHandHoldingUsd class='iCons'/><span> Purchase Tokens</span></Link></li></> : <></>}
                {this.props.roles ? <><li class="sidebar-header">ADMINISTRATOR</li>
                  <li><Link to={"/browse-KYC"}><i class="zmdi zmdi-coffee text-danger"></i> <FaCheck class='iCons'/><span> Browse to the KYC chain</span></Link></li></> : <></>}
              </ul>

            </div>
            <header class="topbar-nav">
              <nav class="navbar navbar-expand fixed-top">
                <ul class="navbar-nav mr-auto align-items-center">
                  <li class="nav-item">
                    <a class="nav-link toggle-menu" href="javascript:void();">
                      <i class="icon-menu menu-icon"></i>
                    </a>
                  </li>
                  <li class="nav-item">
                    <form class="search-bar">
                      <input type="text" class="form-control" placeholder="Enter keywords" />
                      <a href="javascript:void();"><i class="icon-magnifier"></i></a>
                    </form>
                  </li>
                </ul>

                <ul class="navbar-nav align-items-center right-nav-link">
                <Avatar
                    style={{ border: "2px solid gray", margin: 10 }}
                    // alt="GeeksforGeeks Pic 1"
                    src=
                  "https://www.google.com/url?sa=i&url=https%3A%2F%2Fcuoi.tuoitre.vn%2Fzoi-tre%2Fdang-suong-suong-anh-dai-dien-tam-thoi-dai-ca-facebook-gom-ve-co-hon-2-trieu-like-2020070923015944.html&psig=AOvVaw3RWGH1mhflJHuaXGkXarM5&ust=1652103765043000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCMjNzKWE0PcCFQAAAAAdAAAAABAD"
                  />
                  <li class="nav-item dropdown-lg">
                   
                    <a class=" dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
                      {this.props.fullName}</a>
                  </li>

                  <li class="nav-item dropdown-lg ml-3">
                    {/* <a class=" dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
      Logout</a> */}
                    <Link className="dropdown-toggle dropdown-toggle-nocaret waves-effect" onClick={this.logOut}>Logout</Link>
                  </li>

                </ul>
              </nav>
            </header>
            <script src="assets/js/jquery.min.js"></script>

            <div class="clearfix"></div>
            <div class="content-wrapper">
              <div class="container-fluid">

                <div class="row mt-3">
                  <Switch>
                    {/* <ListProduct exact path="/" component={ListProduct} account={this.props.account} products={this.props.products} purchaseProduct={this.props.purchaseProduct}  />
                
                <ProductItMe path="/product-it-me" component={ProductItMe} account={this.props.account} />
                <PurchasedProduct path="/purchased-products" component={PurchasedProduct} account={this.props.account} />
                <OnProduct path="/products" component={OnProduct} account={this.props.account} />
                <SoldProduct path="/sold-products" component={SoldProduct} account={this.props.account} />
                
          
          /> */}
                    <ListProduct exact path="/" component={ListProduct} account={this.props.account} products={this.props.products} purchaseProduct={this.props.purchaseProduct} />
                    <CreateProduct path="/create-product" component={CreateProduct} account={this.props.account} products={this.props.products} createProduct={this.props.createProduct} />
                    <ProductItMe path="/product-it-me" component={ProductItMe} account={this.props.account} />
                    <PurchasedProduct path="/purchased-products" component={PurchasedProduct} account={this.props.account} />
                    <SoldProduct path="/sold-products" component={SoldProduct} account={this.props.account} />
                    <BuyToken path="/buy-tokens" component={BuyToken} account={this.props.account} handleByTokens={this.props.handleByTokens} numbertoken={this.props.numbertoken} />
                    <BrowseKYC path="/browse-KYC" component={BrowseKYC} account={this.props.account} handleKycWhitelisting={this.props.handleKycWhitelisting} />
                  </Switch>

                </div>






                <div class="overlay toggle-menu"></div>

              </div>

            </div>

            <div class="right-sidebar">

            </div>


          </div>

        </body>
      </Router>
    );
  }
}
export default Sidenav