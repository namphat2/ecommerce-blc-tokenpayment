import React, { Component } from "react";
import axios from 'axios';
import Cookies from 'js-cookie'
import imgs from './assets/images/logo-icon.png'
import register from "./signup.component";
class Login extends Component {
  state = {
    password: '',
    error: '',
  }
  handleChange = event => {
    this.setState({ password: event.target.value });
  }
  handleSubmit = event => {
    event.preventDefault();

    const user = {
      'id': this.props.account,
      'password': this.state.password
    };
    console.log(user);
    axios.post(`https://localhost:5000/api/Users/Login`, user)
      .then(res => {
        console.log(res);
        console.log(res.data);
        console.log(res.data.token);
        this.setState({ error: '' })
        Cookies.set('token', res.data.token, { expires: 1 });
        window.location.assign('/');
      })
      .catch(err => {
        console.error(err);
        this.setState({ error: 'Mật khẩu của bạn không chính xác' })
      })
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div class="form-group">
          <label for="exampleInputUsername" class="sr-only">ACCOUNT ADDRESS</label>
          <div class="position-relative has-icon-right">
            <label class='text-login-color'>ACCOUNT ADDRESS</label>
            <input type="text" className="form-control" placeholder="Public Key" readOnly value={this.props.account} />
            <div class="form-control-position">
              <i class="icon-user"></i>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword" class="sr-only">PASSWORD</label>
          <div class="position-relative has-icon-right">
            <label class='text-login-color'>Password</label>
            <input type="password" ref={(input) => { this.productName = input }} name="password" onChange={this.handleChange} className="form-control input-" placeholder="Enter password" />
            <div class="form-control-position">
              <i class="icon-lock"></i>
            </div>
          </div>
        </div>
        <p style={{ color: "red" }}>{this.state.error}</p>
        <div class="form-row">
        </div>
        <button type="submit" class="btn btn-block login-button" style={{color: 'white',fontSize:15}}>Sign In</button>


      </form>

    );
  }
}

export default Login;