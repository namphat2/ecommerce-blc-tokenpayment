import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import { BrowserRouter as Router, Switch, Route, Link,HashRouter } from "react-router-dom";



class BrowseKYC extends Component {
    state = {
        data : [],
    }
    async componentWillMount(){
        await this.listAllProducts();
    }
    listAllProducts(){
      axios.get('https://localhost:5000/api/Users/GetAllUsers', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+Cookies.get('token')
      }
    })
    .then(response => {
      this.setState(this.state.data = response.data);
    })
    .catch(function (error) {
      console.log(error);
      
    })
  
  
}
  render() {
    return (
      <div class="col-lg-12">
          <div class="card list-table">
            <div class="card-body">
              <h4 class="card-title"style={{color:'black', fontWeight: 'bold'}}>List All Users</h4>
			  <div class="table-responsive">
               <table class="table" style={{color:'black'}}>
                  <thead>
                    <tr>
                      <th scope="col"style={{fontSize: 15}}>#</th>
                      <th scope="col"style={{fontSize: 15}}>First Name</th>
                      <th scope="col"style={{fontSize: 15}}>Last Name</th>
                      <th scope="col"style={{fontSize: 15}}>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.data.map((anObjectMapped, index) => {
                        return (
                            <tr key={anObjectMapped.id}>
                                <td>{ anObjectMapped.id}</td>
                            <th scope="row">{anObjectMapped.firstName}</th>
                            <td>{anObjectMapped.lastName}</td>
                            
                            
                            
                            
                            <td><button name={anObjectMapped.productCount} class='login-button' style={{border: 'none',borderRadius: '5px', color: 'white', padding: '5px', marginTop: '-15px'}}
                          value={anObjectMapped.price}
                          onClick={(event) => {
                            this.props.handleKycWhitelisting(anObjectMapped.id)
                          }}> Configure KYC Whitelist</button></td>
                            </tr>
                        );
                        // console.log(productCount)
                        
                    })}
                    
                    
                  </tbody>
                </table>
            </div>
            </div>
          </div>
        </div>
    );
  }
}

export default BrowseKYC;
