import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

class BuyToken extends Component {
  
  render() {
    return (
      <div class="col-lg-12">
         <div class="card list-table">
           <div class="card-body">
           <div class="card-title"style={{color:'black', fontWeight: 'bold', fontSize: 25}}>Buy Token</div>
           <hr />
            <form onSubmit={(event) => {
          event.preventDefault()
          const number = this.number.value
          this.props.handleByTokens(number)
        }}>
           <div class="form-group">
            <label for="input-1" style={{color: 'green', fontWeight: 'bold', fontSize: 13}}>Number Token ({this.props.numbertoken} WEI = 1 TOKEN)</label>
            <input type="number" class="form-control input-" ref={(input) => { this.number = input }} id="number" placeholder="Enter Number Token" required />
           </div>
           
           
           
           <div class="form-group pt-5">
            <button type="submit" class="btn btn-light px-5 login-button"><i class="icon-lock"></i> BUY TOKENS</button>
          </div>
          </form>
         </div>
         </div>
      </div>
    );
  }
}

export default BuyToken;
