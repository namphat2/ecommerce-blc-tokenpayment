import React, { Component } from 'react';

class CreateProduct extends Component {
  
  render() {
    return (
      <div class="col-lg-12">
         <div class="card list-table">
           <div class="card-body">
           <div class="card-title"style={{color:'black', fontWeight: 'bold', fontSize: 25}}>Create Product</div>
           <hr />
            <form onSubmit={(event) => {
          event.preventDefault()
          
          const name = this.productName.value
          const price = this.productPrice.value
          this.props.createProduct(name, price)}}>
           <div class="form-group">
            <label for="input-1" style={{color:'black'}}>Name Product</label>
            <input type="text" class="form-control input-" ref={(input) => { this.productName = input }} id="productName" placeholder="Enter Name Product" required />
           </div>
           <div class="form-group py-2">
            <label for="input-2"style={{color:'black'}}>Price Product (Token)</label>
            <input type="number" class="form-control input-" ref={(input) => { this.productPrice = input }} id="productPrice" placeholder="Enter Price Product" required/>
           </div>
           
           
           <div class="form-group pt-5">
            <button type="submit" class="btn btn-light px-5 login-button"><i class="icon-lock"></i> CREATE PRODUCT</button>
          </div>
          </form>
         </div>
         </div>
      </div>
      
    );
  }
}

export default CreateProduct;
