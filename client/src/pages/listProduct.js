import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import { BrowserRouter as Router, Switch, Route, Link, HashRouter } from "react-router-dom";



class ListProduct extends Component {
  state = {
    data: [],
  }
  async componentWillMount() {
    await this.listAllProducts();
  }
  listAllProducts() {
    axios.get('https://localhost:5000/api/Products', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Cookies.get('token')
      }
    })
      .then(response => {

        // console.log(response);
        this.setState(this.state.data = response.data);
        // console.log(this.state.data);

        // console.log(Object.keys(this.state.data));
        //   this.state.data.forEach( ({ productCount,name,owner,price,isBuy,id }) => {
        //                   console.log(productCount)
        //                   console.log(name)
        //                   console.log(owner)
        //                   console.log(price)
        //                   console.log(isBuy)
        //                   console.log(id)

        //               })
      })
      .catch(function (error) {
        console.log(error);

      })


  }
  render() {
    return (
      <div class="col-lg-12">
        <div class="card list-table">
          <div class="card-body">
            <h4 style={{color:'black', fontWeight: 'bold'}}>List All Product</h4>
            <div class="table-responsive">



              <table class="table" style={{color:'black'}}>
                <thead>
                  <tr>
                    <th scope="col" style={{fontSize: 15}}>#</th>
                    <th scope="col"style={{fontSize: 15}}>Name</th>
                    <th scope="col"style={{fontSize: 15}}>Image</th>
                    <th scope="col"style={{fontSize: 15}}>Price</th>
                    <th scope="col"style={{fontSize: 15}}>Address Product</th>
                    <th scope="col"style={{fontSize: 15}}>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {/* { this.props.products.map((product, key) => {}} */}
                  {/* {this.state.data.forEach( ({ productCount,name,owner,price,isBuy,id }) => {
                        // console.log(productCount)
                        <tr key={id}>
                            <th scope="row">{productCount}</th>
                            <td>{name}</td>
                            <td>{owner}</td>
                            <td>{price}</td>
                            <td>{isBuy}</td>
                            <td>{id}</td>
                        </tr>
                    })} */}
                  {this.state.data.map((anObjectMapped, index) => {
                    return (
                      <tr key={anObjectMapped.id}>
                        <th scope="row">{anObjectMapped.productCount}</th>
                        <td>{anObjectMapped.name}</td>
                        <td>{anObjectMapped.pictureUrl != null ? <a href={"https://localhost:5000" + anObjectMapped.pictureUrl} target="_blank">View Image</a> : ""} </td>
                        <td>{anObjectMapped.price} Token</td>
                        <td>{anObjectMapped.idProduct}

                        </td>


                        <td>{anObjectMapped.owner == this.props.account ? "" : <button class='login-button' style={{border: 'none',borderRadius: '5px', color: 'white', padding: '5px', marginTop: '-15px'}} name={anObjectMapped.productCount}
                          value={anObjectMapped.price}
                          onClick={(event) => {
                            this.props.purchaseProduct(event.target.name, event.target.value, anObjectMapped.owner)
                          }}> Buy Product</button>}</td>
                      </tr>
                    );
                    // console.log(productCount)

                  })}


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ListProduct;
