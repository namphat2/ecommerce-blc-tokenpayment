import React, { Component } from "react";
import axios from 'axios';
import Cookies from 'js-cookie'
import imgs from './assets/images/Marketplace.png'
import register from "./signup.component";
import { BrowserRouter as Router, Switch, Route, Link, HashRouter } from "react-router-dom";
import Login from "./login.component";
import SignUp from "./signup.component";
class Log extends Component {
    render() {
        return (
            <Router>
                <body class="bg-theme bg-theme1">
                    <div id="wrapper">
                        <div class="loader-wrapper"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>
                        <div class="card card-authentication1 mx-auto my-5">
                            <div class="card-body card-login">
                                <div class="card-content p-2">
                                    <div class="text-center">
                                        <img src={imgs} alt="logo icon" />
                                    </div>
                                    <div class="card-title text-uppercase text-center py-3 text-login-color">Ecommerce-blc-tokenpayment</div>
                                    <Switch>
                                        <SignUp path="/sign-up" component={SignUp} account={this.props.account} contractAddress={this.props.contractAddress} />
                                        <Login exact path="/" component={Login} account={this.props.account} />

                                    </Switch>
                                </div>
                            </div>
                            <div class="card-footer text-center py-3 card-login">


                                <p class="text-warning mb-0"> <Link to={"/"} style={{color: 'black', fontWeight: 'bold'}}>LOGIN</Link></p>
                                <p class="text-success mb-0">Do not have an account? <Link to={"/sign-up"} style={{color: 'black', fontWeight: 'bold'}}> REGISTER</Link></p>
                            </div>
                        </div>
                    </div>

                </body>
            </Router>
        );
    }
}

export default Log;